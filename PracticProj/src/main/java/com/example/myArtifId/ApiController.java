package com.example.myArtifId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/{map}", method = RequestMethod.GET)
    public String getCall(@PathVariable("map") String map_v){
        return restTemplate.exchange("http://localhost:9095/api/"+map_v,
                HttpMethod.GET,
                null,
                String.class).getBody();
    }

    @RequestMapping(value = "/{map}", method = RequestMethod.PUT)
    public String putCall(@PathVariable("map") String map_v){
        return restTemplate.exchange("http://localhost:9095/api/"+map_v,
                HttpMethod.PUT,
                null,
                String.class).getBody();
    }

    @RequestMapping(value = "/{map}", method = RequestMethod.POST)
    public String postCall(@PathVariable("map") String map_v){
        return restTemplate.exchange("http://localhost:9095/api/"+map_v,
                HttpMethod.POST,
                null,
                String.class).getBody();
    }

    @RequestMapping(value = "/{map}", method = RequestMethod.PATCH)
    public String patchCall(@PathVariable("map") String map_v){
        return restTemplate.exchange("http://localhost:9095/api/"+map_v,
                HttpMethod.PATCH,
                null,
                String.class).getBody();
    }

    @RequestMapping(value = "/{map}", method = RequestMethod.DELETE)
    public String deleteCall(@PathVariable("map") String map_v){
        return restTemplate.exchange("http://localhost:9095/api/"+map_v,
                HttpMethod.DELETE,
                null,
                String.class).getBody();
    }
}
