package com.example.myArtifId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Controller
public class GreetingController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/")
    public String greeting ( Map<String, Object> model){
        return "greeting";
    }



}
